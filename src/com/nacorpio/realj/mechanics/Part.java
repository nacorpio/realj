package com.nacorpio.realj.mechanics;

public abstract class Part {

	private String id;
	
	public Part(String id) {
		this.id = id;
	}
	
	/**
	 * Returns the unique identifier of this part.
	 * @return the id.
	 */
	public abstract String getId();
	
	/**
	 * Returns an additional object with data.
	 * @return the tag.
	 */
	public abstract Object getTag();
	
	/**
	 * Returns the slot size that this part takes up.
	 * @return the slot size.
	 */
	public abstract int getSlotSize();
	
	/**
	 * Returns the weight in kilograms of this part.
	 * @return the weight.
	 */
	public abstract float getWeight();
	
	/**
	 * Returns the volume of this part in dm3 or liters.
	 * @return the volume.
	 */
	public abstract float getVolume();
	
}
