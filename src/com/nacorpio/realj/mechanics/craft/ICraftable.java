package com.nacorpio.realj.mechanics.craft;

public interface ICraftable {

	/**
	 * Returns the recipes this ICraftable can be crafted from.
	 * @return the recipes.
	 */
	Recipe[] getRecipes();
	
}
