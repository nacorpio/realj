package com.nacorpio.realj.mechanics;

import com.nacorpio.realj.mechanics.craft.ICraftable;
import com.nacorpio.realj.mechanics.craft.Recipe;

public abstract class CraftableAssembly extends Assembly implements ICraftable {

	private Recipe[] recipes;
	
	public CraftableAssembly(String id, Recipe[] r, Part... parts) {
		super(id, parts);
		this.recipes = r;
	}

	public CraftableAssembly(String id, Recipe[] r) {
		super(id);
		this.recipes = r;
	}

	@Override
	protected abstract void initValues();

	@Override
	protected abstract void initParts();

	@Override
	protected abstract void initAssemblies();

	@Override
	public Object getTag() { return getTag().toString().replace("Assembly", "CraftableAssembly"); }

	@Override
	public abstract int getWeightCapacity();

	@Override
	public abstract int getSlotCapacity();

	@Override
	public Recipe[] getRecipes() {
		return recipes;
	}

}
