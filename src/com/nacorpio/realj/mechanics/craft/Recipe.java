package com.nacorpio.realj.mechanics.craft;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Recipe {

	private String id;
	
	public Recipe(String id) {
		this.id = id;
	}

	// 2xIron > 5xScrew
	// 6xIron > 15xScrew
	
	public List<Stack> craft(Stack... input) {
		
		List<Stack> outputs = new ArrayList<Stack>();
		
		for (int i = 0; i < input.length; i++) {
			
			Stack in = input[i];
			
			if (in.getObject().equals(getInput().getObject())) {
				
				// 10 Glass
				int inputStackSize = in.getSize();
				
				// 2 Glass
				int neededInput = getInput().getSize();
				
				// 10 / 2 = 5 products
				int products = (inputStackSize / neededInput);
				
				outputs.add(new Stack(getOutput().getObject(), products));
				
			}
			
		}
		
		return outputs;
		
	}
	
	/**
	 * Returns the Stack of output this recipe will give in return for the input.
	 * @return the output.
	 */
	public abstract Stack getOutput();
	
	/**
	 * Returns the Stack of input this recipe will require to return the output.
	 * @return the input.
	 */
	public abstract Stack getInput();
	
	public final String getId() {
		return id;
	}
	
}
