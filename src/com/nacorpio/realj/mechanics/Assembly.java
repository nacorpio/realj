package com.nacorpio.realj.mechanics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An Assembly is mainly used for structuring real-life objects.<br>
 * For example, you can create a Vehicle class and let the parts of
 * the vehicle be either assemblies, or parts depending on if the
 * object is built together with multiple parts or not.<br>An Assembly 
 * is simply an object that consists of one or more parts.
 * @author Nacorpio
 *
 */
public abstract class Assembly {
	
	private String id;
	
	private boolean child = false;
	private boolean parent = false;
	
	private List<Part> parts = new ArrayList<Part>();
	private List<Assembly> assemblies = new ArrayList<Assembly>();
	
	private Map<Object, Object> values = new HashMap<Object, Object>();
	
	public Assembly(String id, Part... parts) {
		
		this.id = id;
		
		for (Part p: parts) {
			this.parts.add(p);
		}
		
		// Initialization
		initParts();
		initAssemblies();
		initValues();
		
	}
	
	public Assembly(String id) {
		this(id, new Part[] {});
	}
	
	public final Object getValue(Object par1) {
		return values.get(par1);
	}
	
	/**
	 * Initializes all the values.
	 */
	protected abstract void initValues();
	
	/**
	 * Initializes all the parts.
	 */
	protected abstract void initParts();
	
	/**
	 * Intializes all the assemblies.
	 */
	protected abstract void initAssemblies();
	
	/**
	 * Adds a value with the specified key and value.
	 * @param par1 the key.
	 * @param par2 the value.
	 * @return this assembly for chaining.
	 */
	protected final Assembly addValue(Object par1, Object par2) {
		if (!(values.containsKey(par1))) {
			values.put(par1, par2);
		}
		return this;
	}
	
	/**
	 * Adds the specified part to this assembly.
	 * @param par1 the part.
	 * @return this assembly for chaining.
	 */
	protected final Assembly addPart(Part par1) {
		
		for (Part p: parts) {
			
			if (!(p.getId() == par1.getId())) {
				
				if ((hasWeightCapacity() ? getWeight() + p.getWeight() <= this.getWeightCapacity() : true) && (hasSlotCapacity() ? getSlotSize() + p.getSlotSize() <= this.getSlotCapacity() : true) && p.getVolume() <= this.getVolume())
					parent = true;
					parts.add(p);
				
			}
			
		}
		
		return this;
		
	}
	
	/**
	 * Adds the specified assembly to this assembly.
	 * @param par1 the assembly.
	 * @return this assembly for chaining.
	 */
	protected final Assembly addAssembly(Assembly par1) {
		
		for (Assembly a: assemblies) {
			
			if (!(a.getId() == par1.getId())) {
				
				if ((hasWeightCapacity() ? getWeight() + a.getWeight() <= this.getWeightCapacity() : true) && (hasSlotCapacity() ? getSlotSize() + a.getSlotSize() <= this.getSlotCapacity() : true))
					parent = true;
					par1.setIsChild();
					assemblies.add(par1);
				
			}	
			
		}
		
		return this;
		
	}
	
	/**
	 * Returns a {@link Part} with the specified id (if it exists) within this assembly.
	 * @param par1 the id.
	 * @return the specified part.
	 */
	public final Part getPart(String par1) {
		for (Part p: parts) {
			if (p.getId() == par1)
				return p;
		}
		return null;
	}
	
	/**
	 * Returns an {@link Assembly} with the specified id (if it exists) within this assembly.
	 * @param par1 the id.
	 * @return the specified assembly.
	 */
	public final Assembly getAssembly(String par1) {
		for (Assembly a: assemblies) {
			if (a.getId() == par1)
				return a;
		}
		return null;
	}
	
	/**
	 * Returns the tag, used to retrieve this object.
	 * @return the tag.
	 */
	public Object getTag() {
		return "Assembly." + id;
	}
	
	/**
	 * Returns the unique identifier of this assembly.
	 * @return the id.
	 */
	public final String getId() {
		return id;
	}

	/**
	 * Returns all the assemblies of this assembly.
	 * @return the assemblies.
	 */
	public final List<Assembly> getAssemblies() {
		return Collections.unmodifiableList(assemblies);
	}
	
	/**
	 * Returns all the parts of this assembly.
	 * @return the parts.
	 */
	public final List<Part> getParts() {
		return Collections.unmodifiableList(parts);
	}
	
	/**
	 * Returns the net-force of this Assembly in Newtons.
	 * @return the net-force.
	 */
	public final float getNetForce() {
		return getWeight() * 9.82f;
	}
	
	/**
	 * Returns whether there's a weight capacity limit to this assembly.
	 * @return
	 */
	protected final boolean hasWeightCapacity() {
		return getWeightCapacity() == -1;
	}
	
	/**
	 * Returns whether there's a slot capacity limit to this assembly.
	 * @return
	 */
	protected final boolean hasSlotCapacity() {
		return getSlotCapacity() == -1;
	}
	
	/**
	 * Returns whether this assembly has volume capacity for the specified {@link Part} to be added.
	 * @param par1 the part.
	 * @return 
	 */
	protected final boolean hasVolumeCapacityFor(Part par1) {
		return this.getVolume() >= par1.getVolume();
	}
	
	/**
	 * Returns whether this assembly has volume capacity for the specified {@link Assembly} to be added.
	 * @param par1 the assembly.
	 * @return
	 */
	protected final boolean hasVolumeCapacityFor(Assembly par1) {
		return this.getVolume() >= par1.getVolume();
	}
	
	/**
	 * Returns whether this assembly is a child of another Assembly.
	 * @return true/false.
	 */
	public final boolean isChild() {
		return child;
	}
	
	/**
	 * Returns whether this assembly has children.
	 * @return true/false.
	 */
	public final boolean isParent() {
		return parent;
	}
	
	/**
	 * Sets whether this assembly is a child or not.
	 */
	private final void setIsChild() {
		child = !child;
	}
	
	/**
	 * Returns the amount of weight that can be stored on this assembly.<br>
	 * -1 if there's no limit.
	 * @return the weight capacity.
	 */
	public abstract int getWeightCapacity();
	/**
	 * Returns the amount of slots that can be taken on this assembly.<br>
	 * -1 if there's no limit.
	 * @return the slot capacity.
	 */
	public abstract int getSlotCapacity();
	
	/**
	 * Returns the volume in dm3 or liters.
	 * @return the volume.
	 */
	public final float getVolume() {
		float value = 0f;
		for (Part p: parts)
			value += p.getVolume();
		for (Assembly a: assemblies)
			value += a.getVolume();
		return value;
	}
	
	/**
	 * Returns the amount of slots this assembly takes up in total.
	 * @return the slot size.
	 */
	public final int getSlotSize() {
		int value = 0;
		for (Part p: parts)
			value += p.getSlotSize();
		for (Assembly a: assemblies)
			value += a.getSlotSize();
		return value;
	}
	
	/**
	 * Returns the weight of this assembly in kilograms.
	 * @return the weight.
	 */
	public final float getWeight() {
		float value = 0f;
		for (Part p: parts)
			value += p.getWeight();
		for (Assembly a: assemblies)
			value += a.getWeight();
		return value;
	}
	
}
