package com.nacorpio.realj.debug;

import com.nacorpio.realj.mechanics.craft.Recipe;
import com.nacorpio.realj.mechanics.craft.Stack;

public final class Main {

	public static void main(String[] args) {
		
		Recipe r = new Recipe("Recipe.HardenedGlass") {

			@Override
			public Stack getOutput() {
				return new Stack("HARDENED_GLASS", 10);
			}

			@Override
			public Stack getInput() {
				return new Stack("GLASS", 10);
			}
			
		};
		
		Stack s = r.craft(new Stack("GLASS", 20), new Stack("GLASS", 20)).get(1);
		System.out.println(s.getObject().toString() + "x" + s.getSize());
		
	}

}
