package com.nacorpio.realj.mechanics.craft;

public class Stack {

	private Object obj;
	private int size;
	
	public Stack(Object par1, int size) {
		if (size > 0 && par1 != null) {
			this.obj = par1;
			this.size = size;
		}
	}

	public final Object getObject() {
		return obj;
	}
	
	public final int getSize() {
		return size;
	}
	
}
